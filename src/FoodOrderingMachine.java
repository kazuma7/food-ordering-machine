import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton hamburgerButton;
    private JButton cheeseBurgerButton;
    private JButton spicyBeefBurgerButton;
    private JButton eggCheeseBurgerButton;
    private JButton teriyakiBurgerButton;
    private JButton baconLettuceBurgerButton;
    private JButton firstDecrease;
    private JLabel rightLabel;
    private JButton firstIncrease;
    private JLabel priceLabel;
    private JTextPane orderQuantity;
    private JButton secondDecrease;
    private JButton secondIncrease;
    private JButton thirdDecrease;
    private JButton thirdIncrease;
    private JButton fourthDecrease;
    private JButton fourthIncrease;
    private JButton fifthDecrease;
    private JButton fifthIncrease;
    private JButton sixthDecrease;
    private JButton sixthIncrease;
    private JLabel firstOrderQuantity;
    private JLabel secondOrderQuantity;
    private JLabel thirdOrderQuantity;
    private JLabel fourthOrderQuantity;
    private JLabel fifthOrderQuantity;
    private JLabel sixthOrderQuantity;
    private JTextArea orderedItemsList;
    private JButton checkOut;

    int sum = 0;
    int types = 0;

    int all = 6;
    int orderArray[][];

    int priceArray[];
    String currentText = "";


    public FoodOrderingMachine() {
        orderArray = new int[6][2];
        priceArray = new int[6];
        allInitialize();
        priceArray[0] = 150;
        priceArray[1] = 180;
        priceArray[2] = 200;
        priceArray[3] = 220;
        priceArray[4] = 370;
        priceArray[5] = 380;
        orderedItemsList.setText("\n\n");
        hamburgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("HamburgerIcon.jpg")
        ));
        cheeseBurgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("CheeseBurgerIcon.jpg")
        ));
        spicyBeefBurgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("SpicyBeefBurgerIcon.jpg")
        ));
        eggCheeseBurgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("EggCheeseBurgerIcon.jpg")
        ));
        teriyakiBurgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("TeriyakiBurgerIcon.jpg")
        ));
        baconLettuceBurgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("BaconLettuceBurgerIcon.jpg")
        ));
        hamburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderedFood(1,150);

            }
        });
        cheeseBurgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderedFood(2,180);
            }
        });
        spicyBeefBurgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderedFood(3,200);
            }
        });
        eggCheeseBurgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderedFood(4,220);
            }
        });
        teriyakiBurgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderedFood(5,370);
            }
        });
        baconLettuceBurgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderedFood(6,380);
            }

        });
        firstDecrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decrease(0);
            }
        });
        firstIncrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                increase(0);

            }
        });
        secondDecrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decrease(1);
            }
        });
        secondIncrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                increase(1);
            }
        });
        thirdDecrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decrease(2);
            }
        });
        thirdIncrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                increase(2);
            }
        });
        fourthDecrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decrease(3);
            }
        });
        fourthIncrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                increase(3);
            }
        });
        fifthDecrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decrease(4);
            }
        });
        fifthIncrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                increase(4);
            }
        });
        sixthDecrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decrease(5);
            }
        });
        sixthIncrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                increase(5);
            }
        });
        checkOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + summary() + " yen.");
                    allInitialize();
                }
            }
        });
    }

    void orderedFood(int foodNumber, int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + numberToFoodName(foodNumber) + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + numberToFoodName(foodNumber) + "! It will be served as soon as possible.");
            orderArray[types][0] = foodNumber;
            orderArray[types][1] = 1;
            types += 1;
            buttonEnable(types);
            foodButtonDisabled(foodNumber);
            orderVisible(foodNumber, price);

            priceLabel.setText("total   " + summary() + " yen");
        }
    }

    void orderVisible(int foodNumber, int price){
        currentText = orderedItemsList.getText();
        switch(types) {
            case 1:
                orderedItemsList.setText(currentText + numberToFoodName(foodNumber) + "   " + price + "  yen\n\n\n\n");
                firstOrderQuantity.setText("1");
                break;
            case 2:
                orderedItemsList.setText(currentText + numberToFoodName(foodNumber) + "   " + price + "  yen\n\n\n\n");
                secondOrderQuantity.setText("1");
                break;
            case 3:
                orderedItemsList.setText(currentText + numberToFoodName(foodNumber) + "   " + price + "  yen\n\n\n\n");
                thirdOrderQuantity.setText("1");
                break;
            case 4:
                orderedItemsList.setText(currentText + numberToFoodName(foodNumber) + "   " + price + "  yen\n\n\n\n");
                fourthOrderQuantity.setText("1");
                break;
            case 5:
                orderedItemsList.setText(currentText + numberToFoodName(foodNumber) + "   " + price + "  yen\n\n\n\n");
                fifthOrderQuantity.setText("1");
                break;
            case 6:
                orderedItemsList.setText(currentText + numberToFoodName(foodNumber) + "   " + price + "  yen");
                sixthOrderQuantity.setText("1");
                break;
            default:
                break;
        }
    }
    String numberToFoodName(int number){
        switch (number){
            case 1:
                return "Hamburger";
            case 2:
                return "Cheese Burger";
            case 3:
                return "Spicy Beef Burger";
            case 4:
                return "Egg Cheese Burger";
            case 5:
                return "Teriyaki Burger";
            case 6:
                return "Bacon Lettuce Burger";
            default:
                return "";
        }

    }

    void allInitialize(){
        allButtonInvalidate();
        quantityDisabled(0);
        priceLabel.setText("total    yen");
        checkOut.setEnabled(false);
        orderedItemsList.setText("\n\n");
        hamburgerButton.setEnabled(true);
        cheeseBurgerButton.setEnabled(true);
        spicyBeefBurgerButton.setEnabled(true);
        eggCheeseBurgerButton.setEnabled(true);
        teriyakiBurgerButton.setEnabled(true);
        baconLettuceBurgerButton.setEnabled(true);
        types = 0;
        for(int i=0;i<all;i++){
            orderArray[i][0] = 0;
            orderArray[i][1] = 0;
        }
    }
    void allButtonInvalidate(){
        firstDecrease.setVisible(false);
        firstIncrease.setVisible(false);
        secondDecrease.setVisible(false);
        secondIncrease.setVisible(false);
        thirdDecrease.setVisible(false);
        thirdIncrease.setVisible(false);
        fourthDecrease.setVisible(false);
        fourthIncrease.setVisible(false);
        fifthDecrease.setVisible(false);
        fifthIncrease.setVisible(false);
        sixthDecrease.setVisible(false);
        sixthIncrease.setVisible(false);
    }

    void foodButtonDisabled(int foodNumber){
        switch(foodNumber){
            case 1:
                hamburgerButton.setEnabled(false);
                break;
            case 2:
                cheeseBurgerButton.setEnabled(false);
                break;
            case 3:
                spicyBeefBurgerButton.setEnabled(false);
                break;
            case 4:
                eggCheeseBurgerButton.setEnabled(false);
                break;
            case 5:
                teriyakiBurgerButton.setEnabled(false);
                break;
            case 6:
                baconLettuceBurgerButton.setEnabled(false);
                break;
            default:
                break;
        }
    }
    void buttonEnable(int types){
        switch (types){
            case 1:
                firstDecrease.setVisible(true);
                firstIncrease.setVisible(true);
                break;
            case 2:
                secondDecrease.setVisible(true);
                secondIncrease.setVisible(true);
                break;
            case 3:
                thirdDecrease.setVisible(true);
                thirdIncrease.setVisible(true);
                break;
            case 4:
                fourthDecrease.setVisible(true);
                fourthIncrease.setVisible(true);
                break;
            case 5:
                fifthDecrease.setVisible(true);
                fifthIncrease.setVisible(true);
                break;
            case 6:
                sixthDecrease.setVisible(true);
                sixthIncrease.setVisible(true);
                break;
            default:
                break;

        }

    }

    int summary(){
        sum = 0;
        for(int i=0;i<all;i++) {
            switch (orderArray[i][0]) {
                case 1:
                    sum += priceArray[0] * orderArray[i][1];
                    break;
                case 2:
                    sum += priceArray[1] * orderArray[i][1];
                    break;
                case 3:
                    sum += priceArray[2] * orderArray[i][1];
                    break;
                case 4:
                    sum += priceArray[3] * orderArray[i][1];
                    break;
                case 5:
                    sum += priceArray[4] * orderArray[i][1];
                    break;
                case 6:
                    sum += priceArray[5] * orderArray[i][1];
                    break;
                default:
                    break;
            }
        }
        if(sum == 0)
            checkOut.setEnabled(false);
        else
            checkOut.setEnabled(true);
        return sum;
    }

    void increase(int number){
        orderArray[number][1] += 1;
        if(number==0)
            firstOrderQuantity.setText(String.valueOf(orderArray[number][1]));
        else if(number==1)
            secondOrderQuantity.setText(String.valueOf(orderArray[number][1]));
        else if(number==2)
            thirdOrderQuantity.setText(String.valueOf(orderArray[number][1]));
        else if(number==3)
            fourthOrderQuantity.setText(String.valueOf(orderArray[number][1]));
        else if(number==4)
            fifthOrderQuantity.setText(String.valueOf(orderArray[number][1]));
        else if(number==5)
            sixthOrderQuantity.setText(String.valueOf(orderArray[number][1]));

        priceLabel.setText("total   " + summary() + " yen");
    }

    void decrease(int number){
        orderArray[number][1] -= 1;
        if(orderArray[number][1] == 0){
            foodButtonEnabled(orderArray[number][0]);
            orderArray[number][0] = 0;
            types -= 1;
            orderArrayChange(number);
            buttonInvalidate();
            if(number==0){
                textChange();
                firstOrderQuantity.setText((secondOrderQuantity.getText()));
                secondOrderQuantity.setText((thirdOrderQuantity.getText()));
                thirdOrderQuantity.setText((fourthOrderQuantity.getText()));
                fourthOrderQuantity.setText((fifthOrderQuantity.getText()));
                fifthOrderQuantity.setText((sixthOrderQuantity.getText()));
            }
            else if(number==1){
                textChange();
                secondOrderQuantity.setText((thirdOrderQuantity.getText()));
                thirdOrderQuantity.setText((fourthOrderQuantity.getText()));
                fourthOrderQuantity.setText((fifthOrderQuantity.getText()));
                fifthOrderQuantity.setText((sixthOrderQuantity.getText()));
            }
            else if(number==2){
                textChange();
                thirdOrderQuantity.setText((fourthOrderQuantity.getText()));
                fourthOrderQuantity.setText((fifthOrderQuantity.getText()));
                fifthOrderQuantity.setText((sixthOrderQuantity.getText()));
            }
            else if(number==3){
                textChange();
                fourthOrderQuantity.setText((fifthOrderQuantity.getText()));
                fifthOrderQuantity.setText((sixthOrderQuantity.getText()));
            }
            else if(number==4){
                textChange();
                fifthOrderQuantity.setText((sixthOrderQuantity.getText()));
            }
            else if(number==5){
                textChange();
            }
            quantityDisabled(types);
        }
        else{
            if(number==0)
                firstOrderQuantity.setText(String.valueOf(orderArray[number][1]));
            else if(number==1)
                secondOrderQuantity.setText(String.valueOf(orderArray[number][1]));
            else if(number==2)
                thirdOrderQuantity.setText(String.valueOf(orderArray[number][1]));
            else if(number==3)
                fourthOrderQuantity.setText(String.valueOf(orderArray[number][1]));
            else if(number==4)
                fifthOrderQuantity.setText(String.valueOf(orderArray[number][1]));
            else if(number==5)
                sixthOrderQuantity.setText(String.valueOf(orderArray[number][1]));
        }
        priceLabel.setText("total   " + summary() + " yen");
    }

    void quantityDisabled(int types){
        switch(types) {
            case 0:
                firstOrderQuantity.setText("");
            case 1:
                secondOrderQuantity.setText("");
            case 2:
                thirdOrderQuantity.setText("");
            case 3:
                fourthOrderQuantity.setText("");
            case 4:
                fifthOrderQuantity.setText("");
            case 5:
                sixthOrderQuantity.setText("");
        }
    }

    void orderArrayChange(int number){
        for(int i=number;i<all-1;i++) {
            orderArray[i][0] = orderArray[i+1][0];
            orderArray[i][1] = orderArray[i+1][1];
        }
        for(int i=types;i<all;i++){
            orderArray[i][0] = 0;
            orderArray[i][1] = 0;
        }
    }
    void textChange() {
        currentText = "\n\n";
        for (int i = 0; i < types; i++) {
            orderedItemsList.setText(currentText + numberToFoodName(orderArray[i][0]) + "   " + priceArray[orderArray[i][0]-1] + "  yen\n\n\n\n");
            currentText = orderedItemsList.getText();
        }
        if (types == 0)
            orderedItemsList.setText(currentText);
    }

    void foodButtonEnabled(int foodNumber){
        switch(foodNumber){
            case 1:
                hamburgerButton.setEnabled(true);
                break;
            case 2:
                cheeseBurgerButton.setEnabled(true);
                break;
            case 3:
                spicyBeefBurgerButton.setEnabled(true);
                break;
            case 4:
                eggCheeseBurgerButton.setEnabled(true);
                break;
            case 5:
                teriyakiBurgerButton.setEnabled(true);
                break;
            case 6:
                baconLettuceBurgerButton.setEnabled(true);
                break;
            default:
                break;
        }
    }

    void buttonInvalidate(){
            if(types == 0)
                firstButtonInvalidate();
            else if(types == 1)
                secondButtonInvalidate();
            else if(types == 2)
                thirdButtonInvalidate();
            else if(types == 3)
                fourthButtonInvalidate();
            else if(types == 4)
                fifthButtonInvalidate();
            else if(types == 5)
                sixthButtonInvalidate();
    }

    void firstButtonInvalidate(){
        firstDecrease.setVisible(false);
        firstIncrease.setVisible(false);
    }

    void secondButtonInvalidate(){
        secondDecrease.setVisible(false);
        secondIncrease.setVisible(false);
    }

    void thirdButtonInvalidate(){
        thirdDecrease.setVisible(false);
        thirdIncrease.setVisible(false);
    }
    void fourthButtonInvalidate(){
        fourthDecrease.setVisible(false);
        fourthIncrease.setVisible(false);
    }
    void fifthButtonInvalidate(){
        fifthDecrease.setVisible(false);
        fifthIncrease.setVisible(false);
    }
    void sixthButtonInvalidate(){
        sixthDecrease.setVisible(false);
        sixthIncrease.setVisible(false);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
